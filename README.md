0A.D map repository
====================

This is a not official map repository for [0A.D](https://play0ad.com/).

[TODO](TODO.md)


Quick start for developpers
----------------------------

The main programming language is python. Here is how to set up for new developpers.

The environment is also using django and a local database, you will need to set up your database, 
launch the virtual python environment, then run the server.

First setup:

You will need at least python 3.9 installed (with pipenv), then:

```sh
$ cp confs.env.dist confs.env
$ cp secrets.env.dist secrets.env
$ vim -p confs.env secrets.env
$ pipenv sync -d
$ pipenv shell
$ ./manage.py migrate
$ ./manage.py createsuperuser
$ ./manage.py runserverser_plus
```

(fill and memorize your super user)
