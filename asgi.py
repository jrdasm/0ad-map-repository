"""
ASGI config for zeroadmaprepo project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""
from os import environ

from django.core.asgi import get_asgi_application
from dotenv import (
    find_dotenv,
    load_dotenv,
)

load_dotenv(find_dotenv())
environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')
application = get_asgi_application()
