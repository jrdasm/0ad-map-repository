Map format
==========

Zip structure format
--------------------

`my_wonderful_map-v432.zip`:

```
.
 \- mod.json
    \ - name
    \ - version
    \ - label
    \ - dependencies
    \ - [description]
    \ - [url]
 \- maps
     \- skirmishes
          \- map_name.pmp (atlas)
          \- map_name.xml (atlas)
              \ ScriptSettings (atlas)
                 \ - Name
                 \ - Description
                 \ - CircularMap
                 \ - [Keywords]
                 \ - [Preview]
                 \ - [Script]
\- [art/textures/ui/session/icons/mappreview]
     \- map_preview.png[.cached.dds]
```

Note: for random maps, the file names are `map_name.js` and `map_name.json`.

Rules
-----
- 1 map per mod/zip file
- map.settings.name should be unique (first arrived, first served)
- the preview could be a plain png file of 400x300 max size,
  or a DirectDrawSurface file of 512x512 exact size.

Use cases
---------

### Case: one person created a map with Atlas and only have `map_name.xml` and `map_name.pmp` files

Missing data:
- author
- license
- map type (skirmishes, random, scenarios, tutorials)
- preview (upload)
- version (with a default)
- dependencies (with a default)
- big description (in mod.json)
- url
- keywords

Generated data:
- map file name from map.settings.name
- preview file name form map.settings.name
- mod.json name from map.settings.name
- mod.json label from map.settings.description

All those informations could be asked by a wizard

### Case: one person created a map with the pyromod format (zip file with mod.json)

Missing data:
- author
- license
- preview (upload)
- dependencies (with a default)
- big description (edit is possible)
- url
- keywords

Discarded data:
- map file name
- preview file name
- mod.json name
- mod.json label

Generated data:
- map file name from map.settings.name
- preview file name form map.settings.name
- mod.json name from map.settings.name
- mod.json label from map.settings.description

### Case: map uploaded by somone else that the author

Allow to have a free author name in a map, not tight to a User for admin users

Stored and output format
------------------------

variables:
- `name=map.name`
- `norm_name=map.name.lower().replace('[ /]', '_')`
- `version=map.version`
- `desc=map.description`
- `big_desc=map.big_desc`
- `deps=str(map.deps)`
- `url=map.url or ''`
- `keywords=map.keywords or []`

`{norm_name}-{version}.pyromod`:

```
.
 \- mod.json
    \ - name={norm_name}
    \ - version={version}
    \ - label={desc}
    \ - description={big_desc}
    \ - dependencies={deps}
    \ - url={url}
 \- maps
     \- {type}
          \- {norm_name}.pmp
          \- {norm_name}.xml
              \ ScriptSettings
                 \ - Name={name}
                 \ - Description={desc}
                 \ - CircularMap=true
                 \ - Preview={norm_name}.png
                 \ - Keywords={keywords}
                 \ - Script={norm_name}.js
\- [art/textures/ui/session/icons/mappreview]
     \- {norm_name}.png.cached.dds
```
