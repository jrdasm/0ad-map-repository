from allauth.socialaccount.providers.base import ProviderAccount
from allauth.socialaccount.providers.oauth2.provider import OAuth2Provider
"""
https://api.stackexchange.com/docs/types/user
"""


class StackOverflowAccount(ProviderAccount):
    def get_profile_url(self):
        return self.account.extra_data.get("link")

    def get_avatar_url(self):
        return self.account.extra_data.get("profile_image")

    def to_str(self):
        return self.account.extra_data.get("display_name", super().to_str())


class StackOverflowProvider(OAuth2Provider):
    id = "stackoverflow"
    name = "StackOverflow"
    account_class = StackOverflowAccount

    def extract_uid(self, data):
        return str(data["account_id"])

    def extract_common_fields(self, data):
        return dict(
            username=data.get("link").split('/')[-1],
            name=data.get("display_name"),
        )


provider_classes = [StackOverflowProvider]
