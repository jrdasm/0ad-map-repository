from allauth.socialaccount.providers.oauth2.urls import default_urlpatterns

from .provider import StackOverflowProvider

urlpatterns = default_urlpatterns(StackOverflowProvider)
