from functools import partial
from pathlib import Path
from typing import (
    BinaryIO,
    Callable,
    Optional,
)

from django.core.exceptions import ValidationError
from django.utils.deconstruct import deconstructible
from django.utils.translation import gettext_lazy as _
from magic import from_buffer as magic_from_buffer
from PIL import (
    Image,
    UnidentifiedImageError,
)

from .map_parser import MapParser
from .models.category import CategoryType


@deconstructible
class FileMimeValidator:
    messages = {
        "malicious_file": _("File looks malicious. Allowed extensions are: '%(allowed_extensions)s'."),
        "not_supported": _(
            "File extension '%(extension)s' is not allowed. "
            "Allowed extensions are: '%(allowed_extensions)s'."
        )
    }
    code = 'invalid_extension'
    ext_content_mapping = {
        'jpg': 'image/jpeg',
        'jpeg': 'image/jpeg',
        'png': 'image/png'
    }

    def __init__(self, ext_content_mapping: dict[str, str] = None):
        if ext_content_mapping is not None:
            self.ext_content_mapping = ext_content_mapping
        self.allowed_extensions = set(ext.lower() for ext in self.ext_content_mapping)

    def _buildNotSupportedException(self, extension) -> ValidationError:
        return ValidationError(
            self.messages['not_supported'],
            code=self.code,
            params={
                'extension': extension,
                'allowed_extensions': ', '.join(self.allowed_extensions),
            },
        )

    def _buildMaliciousFileException(self) -> ValidationError:
        return ValidationError(
            self.messages['malicious_file'],
            code=self.code,
            params={
                'allowed_extensions': ', '.join(self.allowed_extensions),
            },
        )

    def __call__(self, data: BinaryIO):
        extension = Path(data.name).suffix[1:].lower()
        content_type = magic_from_buffer(data.read(1024), mime=True)
        if extension not in self.allowed_extensions:
            raise self._buildNotSupportedException(extension)
        if content_type != self.ext_content_mapping[extension]:
            raise self._buildMaliciousFileException()

    def __eq__(self, other: object) -> bool:
        return all((
            isinstance(other, self.__class__),
            self.allowed_extensions == other.allowed_extensions,
            self.message == other.message,
            self.code == other.code,
        ))


@deconstructible
class ZipMapValidator:
    def __init__(self, only_cat: Optional[CategoryType] = None):
        self.only_cat = only_cat

    def __call__(self, data: BinaryIO):
        MapParser(data).check_valid(self.only_cat)
        data.seek(0)

    def __eq__(self, other: object) -> bool:
        return isinstance(other, self.__class__) and self.only_cat == other.only_cat


def _validate_map(value_file: BinaryIO, only_cat: Optional[CategoryType] = None) -> None:
    """
    Should be a zip file zith extension .zip or .pyromod
    It should also contains plain zipped map files or a pyrogenesis mod structure
    """
    FileMimeValidator(ext_content_mapping={
        'pyromod': 'application/zip',
        'zip': 'application/zip',
    })(value_file)
    ZipMapValidator(only_cat)(value_file)


def validate_map_factory(only_cat: Optional[CategoryType] = None) -> Callable[[BinaryIO], None]:
    """
    Should be a zip file zith extension .zip or .pyromod
    It should also contains plain zipped map files or a pyrogenesis mod structure
    If only_cat is provided, only this map category is considered valid
    """
    return partial(_validate_map, only_cat=only_cat)


def validate_thumbnail(value_file: BinaryIO) -> None:
    """
    Should be a PNG with max size MapParser.preview_size
    """
    if value_file:
        file_mime_validator = FileMimeValidator(ext_content_mapping={
            'png': 'image/png',
        })
        file_mime_validator(value_file)
        try:
            img = Image.open(value_file)
            if img.format != 'PNG':
                raise file_mime_validator._buildMaliciousFileException()
            if img.size > MapParser.preview_size:
                raise file_mime_validator._buildMaliciousFileException()
        except UnidentifiedImageError:
            raise file_mime_validator._buildMaliciousFileException()
        value_file.seek(0)


validate_thumbnail.max_size = MapParser.preview_size
