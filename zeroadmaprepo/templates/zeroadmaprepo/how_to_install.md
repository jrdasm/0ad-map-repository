How to install a map on 0ad?
============================

Download the map you want from the web site. You will get a **`.pyromod`** file.

Here is the help to install this file depending on the *Operation System* you use.
 
 
On Windows
----------
Double-click on the **`pyromod`** file, Windows will ask which application to use, go to `Program files` and select `0ad`.


On Linux
--------
Right-click on the **`pyromod`** file, select `open with` then select `0ad`.


On Macos
--------
*TODO*


Then
----
To finish the installation, the map needs to be enabled.

Select in the upper screen the map mod to enable, then click on the button **`enable`**.

[![enable your map/mod](static/img/select-mod.png){:width="100%"}](static/img/select-mod.png){:style="width:80%; display: block; margin: auto;"}

The map mod is then on the bottom screen, you need to click on **`Save and restart`**.

[![save and restart](static/img/selected-mod.png){:width="100%"}](static/img/selected-mod.png){:style="width:80%; display: block; margin: auto;"}

After a restart your new map is now available in 0ad and will stay in the game.
