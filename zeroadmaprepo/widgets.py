from django.forms import widgets


class PreviewImageInput(widgets.ClearableFileInput):
    template_name = 'zeroadmaprepo/widgets/preview_image_input.html'
