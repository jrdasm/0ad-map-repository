from allauth.account.forms import SignupForm
from django import forms
from django.utils.translation import gettext_lazy as _
from django_json_widget.widgets import JSONEditorWidget
from tagulous.forms import TagField
from tagulous.models import TagOptions

from .models import Release
from .models.category import CategoryType
from .models.license import LicenseType
from .validators import (
    validate_map_factory,
    validate_thumbnail,
)
from .widgets import PreviewImageInput

_first_name = "first name"
_last_name = "last name"


class CustomSignupForm(SignupForm):
    first_name = forms.CharField(
        required=False,
        label=_(_first_name).capitalize(),
    )
    last_name = forms.CharField(
        required=False,
        label=_(_last_name).capitalize(),
    )


class RlzCreateStep1Form(forms.Form):
    """
    Should be a zip file containing mod.json, maps/TYPE/MAP.{pmp,xml,js,json}
    and optionaly art/textures/ui/session/icons/mappreview/MAP.png[.cached.dds] files.
    OR
    Should be a zip file containing MAP.{pmp,xml,js,json}
    and optionaly MAP.png files.
    """
    def __init__(self, *args, **kwargs):
        only_cat = kwargs.pop('only_cat', None)
        super().__init__(*args, **kwargs)
        self.fields['zip_file'].validators = [validate_map_factory(only_cat)]

    zip_file = forms.FileField()


class RlzCreateStep2Form(forms.Form):
    summary = forms.CharField(
        max_length=255,
        help_text=_("Summary for the map."),
        widget=forms.Textarea(attrs=dict(rows=3)),
    )
    description = forms.CharField(
        widget=forms.Textarea,
        help_text=_("Complete description of the map."),
    )
    version = forms.CharField(
        initial='1.0.0',
        max_length=3+1+3+1+3,
        help_text=_("Semver version."),
    )
    new_thumbnail = forms.ImageField(
        required=False,
        widget=PreviewImageInput,
        help_text=_("It should be a %(size)s PNG.") % {'size': 'x'.join(str(x) for x in validate_thumbnail.max_size)},
    )
    dependencies = forms.JSONField(
        initial=["0ad"],
        help_text=_("Dependency list."),
        widget=JSONEditorWidget(width='30em', height='10em', options={
            'modes': ['code', 'tree'],
            'mode': 'tree',
            'navigationBar': False,
            'statusBar': False,
            'search': False,
            'enableSort': False,
            'enableTransform': False,
        }),
    )
    url = forms.URLField(
        required=False,
    )
    keywords = TagField(
        required=False,
        tag_options=TagOptions(
            space_delimiter=False,
            verbose_name_singular="keyword",
            autocomplete_settings={'placeholder': ''},
        ),
        help_text=_("Use comma or enter to separate keywords"),
    )


class RlzEditForm(forms.ModelForm):
    thumbnail = forms.ImageField(
        required=True,
        help_text=Release._meta.get_field('thumbnail').help_text,
        validators=Release._meta.get_field('thumbnail').validators,
        widget=PreviewImageInput,
    )
    dependencies = forms.JSONField(
        help_text=_("Dependency list."),
        widget=JSONEditorWidget(width='30em', height='10em', options={
            'modes': ['code', 'tree'],
            'mode': 'tree',
            'navigationBar': False,
            'statusBar': False,
            'search': False,
            'enableSort': False,
            'enableTransform': False,
        }),
    )
    keywords = TagField(
        required=False,
        tag_options=TagOptions(
            space_delimiter=False,
            verbose_name_singular="keyword",
            autocomplete_settings={'placeholder': ''},
        ),
        help_text=_("Use comma or enter to separate keywords"),
    )

    class Meta:
        model = Release
        fields = ('version', 'summary', 'description', 'thumbnail', 'dependencies', 'url', 'keywords')
        widgets = {
            'summary': forms.Textarea(attrs=dict(rows=3)),
        }


class MapCreateStep1Form(RlzCreateStep1Form):
    pass


class MapCreateStep2Form(RlzCreateStep2Form):
    name = forms.CharField(
        max_length=100,
        help_text=_("Short name for the map, should be unique."),
    )
    slug = forms.SlugField(
        max_length=100,
        help_text=_("Short name for the map, only letter, numbers, underscores and hyphens, should be unique."),
    )
    category = forms.ChoiceField(
        choices=CategoryType.choices,
        help_text=_("Type of the map."),
    )
    license = forms.ChoiceField(
        choices=LicenseType.choices,
        initial=LicenseType.MIT.value,
        help_text=_("License type."),
    )
    field_order = ('name', 'slug', 'category', 'license')


class MapCreateStep2AdminForm(MapCreateStep2Form):
    author_override = forms.CharField(
        required=False,
        max_length=100,
        label=_("Author"),
        help_text=_("Author override, instead of '%(user_name)s'"),
    )
    field_order = ('name', 'slug', 'category', 'license', 'author_override')
