from django import template
from django.templatetags.i18n import (
    do_block_translate,
    do_translate,
)

register = template.Library()


@register.tag("translate_done")
@register.tag("trans_done")
def translate_done(parser, token):
    return do_translate(parser, token)


@register.tag("blocktranslate_done")
@register.tag("blocktrans_done")
def block_translate_done(parser, token):
    return do_block_translate(parser, token)
