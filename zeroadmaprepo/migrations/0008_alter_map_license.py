# Generated by Django 3.2.7 on 2021-10-01 21:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('zeroadmaprepo', '0007_alter_user_email'),
    ]

    operations = [
        migrations.AlterField(
            model_name='map',
            name='license',
            field=models.CharField(choices=[('0bsd', 'BSD Zero Clause License'), ('afl-3.0', 'Academic Free License v3.0'), ('agpl-3.0', 'GNU Affero General Public License v3.0'), ('apache-2.0', 'Apache License 2.0'), ('bsd-2-clause', 'BSD 2-Clause “Simplified” License'), ('cc0-1.0', 'Creative Commons Zero v1.0 Universal'), ('cc-by-4.0', 'Creative Commons Attribution 4.0 International'), ('cc-by-sa-4.0', 'Creative Commons Attribution Share Alike 4.0 International'), ('cecill-2.1', 'CeCILL Free Software License Agreement v2.1'), ('eupl-1.2', 'European Union Public License 1.2'), ('gpl-2.0', 'GNU General Public License v2.0'), ('gpl-3.0', 'GNU General Public License v3.0'), ('isc', 'ISC License'), ('lgpl-2.1', 'GNU Lesser General Public License v2.1'), ('lgpl-3.0', 'GNU Lesser General Public License v3.0'), ('mit-0', 'MIT No Attribution'), ('mit', 'MIT License'), ('osl-3.0', 'Open Software License 3.0'), ('unlicense', 'The Unlicense'), ('wtfpl', 'Do What The Fuck You Want To Public License')], default='mit', help_text='License type.', max_length=32),
        ),
    ]
