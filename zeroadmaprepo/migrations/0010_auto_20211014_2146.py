# Generated by Django 3.2.8 on 2021-10-14 21:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('zeroadmaprepo', '0009_auto_20211004_0739'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='map',
            options={'ordering': ['name'], 'permissions': [('add_release', 'Can add release')]},
        ),
        migrations.AlterModelOptions(
            name='release',
            options={'ordering': ['-version']},
        ),
        migrations.AlterField(
            model_name='release',
            name='modified',
            field=models.DateTimeField(auto_now=True, verbose_name='last_modified_date'),
        ),
    ]
