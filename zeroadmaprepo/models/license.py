from typing import (
    TYPE_CHECKING,
    Optional,
    TypeVar,
)

from django.db import models
from django.template.loader import render_to_string

if TYPE_CHECKING:
    from .map import Map
T = TypeVar("T", bound="LicenseType")


class LicenseType(models.TextChoices):
    BSD0 = '0bsd', "BSD Zero Clause License"
    AFL = 'afl-3.0', "Academic Free License v3.0"
    AGPL = 'agpl-3.0', "GNU Affero General Public License v3.0"
    APACHE = 'apache-2.0', "Apache License 2.0"
    BSD = 'bsd-2-clause', "BSD 2-Clause “Simplified” License"
    CC0 = 'cc0-1.0', "Creative Commons Zero v1.0 Universal"
    CCBY = 'cc-by-4.0', "Creative Commons Attribution 4.0 International"
    CCBYSA = 'cc-by-sa-4.0', "Creative Commons Attribution Share Alike 4.0 International"
    CECILL = 'cecill-2.1', "CeCILL Free Software License Agreement v2.1"
    EUPL = 'eupl-1.2', "European Union Public License 1.2"
    GPL2 = 'gpl-2.0', "GNU General Public License v2.0"
    GPL3 = 'gpl-3.0', "GNU General Public License v3.0"
    ISC = 'isc', "ISC License"
    LGPL2 = 'lgpl-2.1', "GNU Lesser General Public License v2.1"
    LGPL3 = 'lgpl-3.0', "GNU Lesser General Public License v3.0"
    MIT0 = 'mit-0', "MIT No Attribution"
    MIT = 'mit', "MIT License"
    OSL = 'osl-3.0', "Open Software License 3.0"
    UNLICENSE = 'unlicense', "The Unlicense"
    WTFPL = 'wtfpl', "Do What The Fuck You Want To Public License"

    @classmethod
    def from_value(cls: type[T], value: str) -> Optional[T]:
        for e in cls:
            if e.value == value:
                return e
        return None

    def url(self) -> str:
        norm_name = str(self.value).lower().replace('_', '-')
        return f"https://choosealicense.com/licenses/{norm_name}"

    def text(self, map: 'Map') -> str:
        ctx = dict(project=map.name, fullname=map.author_name, year=map.created.year)
        return render_to_string(f'licenses/{self.value}.txt', ctx)
