from typing import (
    Optional,
    TypeVar,
)

from django.db import models
from django.utils.translation import gettext_lazy as _

T = TypeVar("T", bound="CategoryType")


class CategoryType(models.TextChoices):
    SKIRMISH = 'skirmishes', _("Skirmish")
    SCENARIO = 'scenarios', _("Scenario")
    TUTORIAL = 'tutorials', _("Tutorial")
    RANDOM = 'random', _("Random")

    @classmethod
    def from_value(cls: type[T], value: str) -> Optional[T]:
        for e in cls:
            if e.value == value:
                return e
        return None
