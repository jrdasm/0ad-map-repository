from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from .category import CategoryType
from .license import LicenseType
from .user import User


class Map(models.Model):
    active = models.BooleanField(default=True)
    name = models.CharField(
        max_length=100,
        unique=True,
        help_text=_("Name for the map, should be unique."),
    )
    slug = models.SlugField(
        max_length=100,
        unique=True,
        help_text=_("Short name for the map, only letter, numbers, underscores and hyphens, should be unique."),
    )
    category = models.CharField(
        max_length=10,
        choices=CategoryType.choices,
        help_text=_("Type of the map."),
    )
    author = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    free_author = models.CharField(
        max_length=100,
        null=True,
        blank=True,
    )
    license = models.CharField(
        max_length=32,
        choices=LicenseType.choices,
        default=LicenseType.MIT,
        help_text=_("License type."),
    )
    last_release = models.ForeignKey('Release', on_delete=models.SET_NULL, null=True, blank=True, related_name='+')
    created = models.DateTimeField(auto_now_add=True, verbose_name='creation_date')
    modified = models.DateTimeField(auto_now=True, verbose_name='laste_modified_date')

    @property
    def author_name(self):
        return self.author.name if self.author else (self.free_author or '−')

    def _get_attr_from_release(self, attr, default=None):
        return getattr(self.last_release, attr) if self.last_release else default

    @property
    def category_type(self) -> CategoryType:
        return CategoryType.from_value(self.category)

    @property
    def license_type(self) -> LicenseType:
        return LicenseType.from_value(self.license)

    @property
    def summary(self):
        return self._get_attr_from_release('summary')

    @property
    def description(self):
        return self._get_attr_from_release('description')

    @property
    def data(self):
        return self._get_attr_from_release('data')

    @property
    def thumbnail(self):
        return self._get_attr_from_release('thumbnail')

    @property
    def dependencies(self):
        return self._get_attr_from_release('dependencies')

    @property
    def url(self):
        return self._get_attr_from_release('url')

    @property
    def keywords(self):
        return self._get_attr_from_release('keywords_list')

    def get_absolute_url(self):
        return reverse('map-detail', args=(self.pk, self.slug))

    def __str__(self):
        return f"{self.name} - {self.get_category_display()}"

    class Meta:
        ordering = ['name']
        permissions = [
            ("add_release", "Can add release"),
        ]
