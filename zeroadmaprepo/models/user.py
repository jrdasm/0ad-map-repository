from django.contrib.auth.models import (
    AbstractUser,
    Permission,
)
from django.db import models
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from django.utils.translation import gettext_lazy as _done


class User(AbstractUser):
    first_name = models.CharField(_done('first name'), max_length=150, blank=True, null=True)
    last_name = models.CharField(_done('last name'), max_length=150, blank=True, null=True)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = f"{self.first_name or ''} {self.last_name or ''}"
        return full_name.strip()

    @property
    def is_social_account(self):
        return bool(self.socialaccount_set.count())

    @property
    def name(self):
        return self.get_full_name() or self.get_username()

    @property
    def name_ext(self):
        ext_social = _("social account")
        ext_0ad = _("0ad account")
        return f"{self.name} ({ext_social if self.is_social_account else ext_0ad})"

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name_ext


@receiver(models.signals.post_save, sender=User)
def fix_perms_for_user(sender: type, instance: User, created: bool, **kwargs) -> bool:
    user = instance
    if created and not user.is_superuser and user != User.get_anonymous():
        user.user_permissions.add(Permission.objects.get(codename='add_map'))
    return True
