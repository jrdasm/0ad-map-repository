from hashlib import sha256 as hashfct
from pathlib import Path

from django.db import models
from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from tagulous.models import TagField

from ..validators import validate_thumbnail


def hash_from_name(name: str, ext: str = None) -> Path:
    h = hashfct(name.encode('utf8')).hexdigest()
    return Path(h[0]) / h[1] / (h[2:] + (f".{ext}" if ext else h[2:]))


def upload_to_hash_maps(instance: 'Release', filename: str = None) -> Path:
    hash_path = hash_from_name(instance.map.name, 'pyromod')
    return Path('maps') / hash_path


def upload_to_hash_thumbnails(instance: 'Release', filename: str = None) -> Path:
    hash_path = hash_from_name(instance.map.name, 'png')
    return Path('thumbnails') / hash_path


class Release(models.Model):
    map = models.ForeignKey('Map', on_delete=models.CASCADE, related_name='releases')
    version = models.CharField(
        max_length=3+1+3+1+3,
        help_text=_("Semver version."),
    )
    summary = models.CharField(
        max_length=255,
        help_text=_("Summary for the map."),
    )
    description = models.TextField(
        help_text=_("Complete description of the map."),
    )
    data = models.FileField(
        upload_to=upload_to_hash_maps,
        verbose_name='map',
        help_text=_(
            "Should be a zip file containing maps/TYPE/MAP.pmp, maps/TYPE/MAP.xml "
            "and optionaly art/textures/ui/session/icons/mappreview/MAP.png files."
        ),
    )
    thumbnail = models.ImageField(
        upload_to=upload_to_hash_thumbnails,
        null=True,
        blank=True,
        help_text=_("It should be a %(size)s PNG.") % {'size': 'x'.join(str(x) for x in validate_thumbnail.max_size)},
        validators=[validate_thumbnail],
    )
    dependencies = models.JSONField(
        help_text=_("Dependency list."),
    )
    url = models.URLField(
        null=True,
        blank=True,
    )
    keywords = TagField(space_delimiter=False, verbose_name_singular="keyword")
    created = models.DateTimeField(auto_now_add=True, verbose_name='creation_date')
    modified = models.DateTimeField(auto_now=True, verbose_name='last_modified_date')

    @property
    def keywords_list(self):
        return self.keywords.get_tag_list()

    def get_absolute_url(self) -> str:
        return reverse('release-detail', args=(self.map.pk, self.map.slug, self.version))

    def __str__(self) -> str:
        return f"{self.map} {self.version}"

    class Meta:
        ordering = ['-version']


@receiver(models.signals.pre_save, sender=Release)
def delete_old_files(sender: type, instance: Release, **kwargs) -> bool:
    release = instance
    if not release.pk:  # creation
        return False
    try:
        old_model = Release.objects.get(pk=release.pk)
    except Release.DoesNotExist:
        return False
    if old_model.data and old_model.data != release.data:
        Path(old_model.data.path).unlink(missing_ok=True)
    if old_model.thumbnail and old_model.thumbnail != release.thumbnail:
        Path(old_model.thumbnail.path).unlink(missing_ok=True)
    return True
