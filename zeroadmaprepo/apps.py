from django.apps import AppConfig


class ZeroadmaprepoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'zeroadmaprepo'
    verbose_name = '0AD map repository'
