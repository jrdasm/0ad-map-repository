from allauth.account.adapter import DefaultAccountAdapter


class CustomAccountAdapter(DefaultAccountAdapter):
    """
    https://django-allauth.readthedocs.io/en/latest/advanced.html#custom-user-models
    """
