from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from guardian.admin import GuardedModelAdmin

from .models import (
    Map,
    Release,
    User,
)

admin.site.register(User, UserAdmin)


@admin.register(Map)
class MapAdmin(GuardedModelAdmin):
    exclude = ('created', 'modified')
    list_display = ('name', 'author', 'free_author', 'active', 'modified')
    list_filter = (
        ('author', admin.RelatedOnlyFieldListFilter),
        'free_author',
        'active',
        'modified',
    )
    date_hierarchy = 'modified'
    ordering = ('name',)
    search_fields = ('name', 'author__username', 'free_author')


@admin.register(Release)
class ReleaseAdmin(GuardedModelAdmin):
    exclude = ('created', 'modified')
    list_display = ('map', 'version', 'modified')
    list_filter = (
        ('map', admin.RelatedOnlyFieldListFilter),
        'version',
        'modified',
    )
    date_hierarchy = 'modified'
    ordering = ('map', 'version')
    search_fields = ('map__name', 'version')
