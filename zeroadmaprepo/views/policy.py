from .markdown import MarkDownView


class PolicyView(MarkDownView):
    md_filename = 'policy.md'
