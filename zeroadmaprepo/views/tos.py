from .markdown import MarkDownView


class TosView(MarkDownView):
    md_filename = 'tos.md'
