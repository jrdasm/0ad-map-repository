from .markdown import MarkDownView


class HowToInstallView(MarkDownView):
    md_filename = 'how_to_install.md'
