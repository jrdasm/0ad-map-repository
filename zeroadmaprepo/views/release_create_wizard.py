from base64 import b64encode
from io import BytesIO
from uuid import uuid1

from django.conf import settings
from django.core.files.base import File
from django.core.files.images import ImageFile
from django.db import transaction
from django.http import Http404
from django.shortcuts import (
    get_object_or_404,
    redirect,
)
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views import View
from django.views.generic import FormView
from guardian.mixins import PermissionRequiredMixin
from guardian.shortcuts import assign_perm

from ..forms import (
    RlzCreateStep1Form,
    RlzCreateStep2Form,
)
from ..map_parser import MapParser
from ..models import (
    Map,
    Release,
)
from ..models.category import CategoryType
from .map_create_wizard import MapCreateStep1View


class ReleaseCreatePermsMixin(PermissionRequiredMixin):
    permission_required = 'zeroadmaprepo.add_release'

    def get_permission_object(self):
        self.map = get_object_or_404(Map, pk=self.kwargs['pk'])
        return self.map


class ReleaseCreateView(ReleaseCreatePermsMixin, View):
    def get(self, request, *args, **kwargs):
        return redirect('release-create-step1', self.kwargs['pk'], self.kwargs['sname'], str(uuid1()))


class ReleaseCreateStep1View(ReleaseCreatePermsMixin, MapCreateStep1View):
    form_class = RlzCreateStep1Form
    template_name = 'zeroadmaprepo/release_create_step1.html'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # can only upload pyromod or plain zip compatible to map category
        kwargs['only_cat'] = CategoryType.from_value(self.map.category)
        return kwargs

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['map'] = self.map
        ctx['title'] = _("Add a map release to %(map_name)s") % {'map_name': self.map.name}
        return ctx

    def get_success_url(self):
        return reverse_lazy('release-create-step2', kwargs=self.kwargs)


class ReleaseCreateStep2View(ReleaseCreatePermsMixin, FormView):
    form_class = RlzCreateStep2Form
    template_name = 'zeroadmaprepo/release_create_step2.html'

    def get_initial(self):
        initial = super().get_initial()
        uuid = self.kwargs['uuid']
        zip_file_path = settings.TMP_ROOT / f"{uuid}.zip"
        if not zip_file_path.exists():
            raise Http404
        json = self.request.session[uuid]
        if json:
            initial['summary'] = json['map_settings']['Description']
            if not initial['summary']:
                initial['summary'] = self.map.summary
            if json['mod']:
                initial['description'] = json['mod']['description']
                initial['version'] = json['mod']['version']
                initial['dependencies'] = json['mod']['dependencies']
                initial['url'] = json['mod']['url']
            if not initial.get('description'):
                initial['description'] = self.map.description
            if not initial.get('version'):
                # update minor digit in semver version
                ver_list = self.map.last_release.version.split('.')
                pos = 1 if len(ver_list) >= 2 else 0
                ver_list[pos] = str(int(ver_list[pos]) + 1)
                initial['version'] = '.'.join(ver_list)
            if not initial.get('dependencies'):
                initial['dependencies'] = self.map.dependencies
            if not initial.get('url'):
                initial['url'] = self.map.url
            initial['keywords'] = json['map_settings']['Keywords']
            if not initial['keywords']:
                initial['keywords'] = self.map.keywords
        return initial

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        uuid = self.kwargs['uuid']
        ctx['uuid'] = uuid
        ctx['map'] = self.map
        ctx['title'] = _("Add a map release to %(map_name)s") % {'map_name': self.map.name}
        json = self.request.session[uuid]
        ctx['warnings'] = json['warnings']
        map_preview_path = settings.TMP_ROOT / f"{uuid}.png"
        if map_preview_path.exists():
            map_preview_base64 = b64encode(map_preview_path.read_bytes()).decode('ascii')
            ctx['original_thumbnail'] = f"data:image/png;base64,{map_preview_base64}"
        else:
            ctx['original_thumbnail'] = None
        return ctx

    def form_valid(self, form):
        try:
            with transaction.atomic():
                uuid = self.kwargs['uuid']
                initial_json = self.request.session[uuid]
                form_data = form.cleaned_data
                map_slug = self.map.slug
                map_settings = initial_json['map_settings'].copy()
                map_settings.update({
                    'Name': self.map.name,
                    'Description': form_data['summary'],
                    'Keywords': form_data['keywords'],
                    'Preview': f"{map_slug}.png",
                })
                if 'Script' in map_settings:
                    map_settings['Script'] = f"{map_slug}.js"
                mod = (initial_json['mod'] or {}).copy()
                mod.update({
                    'name': map_slug,
                    'label': form_data['summary'],
                    'description': form_data['description'],
                    'version': form_data['version'],
                    'dependencies': form_data['dependencies'],
                })
                if form_data['url']:
                    mod['url'] = form_data['url']
                json = {
                    'mod_format': initial_json['mod_format'],
                    'mod': mod,
                    'map_name': map_slug,
                    'map_ext': initial_json['map_ext'],
                    'map_cat': self.map.category,
                    'mod_license': self.map.license,
                    'zip_path_list': initial_json['zip_path_list'],
                    'zip_map_info': initial_json['zip_map_info'],
                    'map_settings': map_settings,
                    'map_preview_path': initial_json['map_preview_path'],
                    'script_path': initial_json['script_path'],
                    'warnings': [],
                }
                zip_file_path = settings.TMP_ROOT / f"{uuid}.zip"
                map_preview_path = settings.TMP_ROOT / f"{uuid}.png"
                with zip_file_path.open('rb') as zip_file:
                    if form_data['new_thumbnail']:
                        preview = form_data['new_thumbnail']
                    else:
                        preview = BytesIO(map_preview_path.read_bytes())
                    map_parser = MapParser(zip_file)
                    map_parser.from_json(json)
                    pyromod = map_parser.build_pyromod(self.map, preview)
                release = Release(
                    map=self.map,
                    version=form_data['version'],
                    summary=form_data['summary'],
                    description=form_data['description'],
                    dependencies=form_data['dependencies'],
                    url=form_data['url'],
                    keywords=form_data['keywords'],
                )
                release.data.save(
                    Release._meta.get_field('data').upload_to(release),
                    File(pyromod),
                    save=False,
                )
                release.thumbnail.save(
                    Release._meta.get_field('thumbnail').upload_to(release),
                    ImageFile(preview),
                    save=False,
                )
                release.save()
                self.map.last_release = release
                self.map.save()
                zip_file_path.unlink(missing_ok=True)
                map_preview_path.unlink(missing_ok=True)
                self.release = release
                assign_perm('zeroadmaprepo.change_release', self.request.user, release)
                assign_perm('zeroadmaprepo.delete_release', self.request.user, release)
                return super().form_valid(form)
        except Exception as e:
            form.add_error(None, str(e))
            return self.form_invalid(form)

    def get_success_url(self):
        return reverse_lazy('release-detail', kwargs={
            'pk': self.release.map.pk,
            'sname': self.release.map.slug,
            'version': self.release.version,
        })
