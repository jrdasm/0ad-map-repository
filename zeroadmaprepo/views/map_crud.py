from django.core.files.base import File
from django.db import transaction
from django.db.models import Q
from django.forms.models import modelform_factory
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views import View
from django.views.generic import (
    DeleteView,
    DetailView,
    ListView,
    UpdateView,
)
from guardian.mixins import PermissionRequiredMixin

from ..map_parser import MapParser
from ..models import (
    Map,
    Release,
)


class ActiveMapMixin:
    def get_queryset(self):
        return super().get_queryset().filter(active=True)


class MapListView(ActiveMapMixin, ListView):
    model = Map
    paginate_by = 100
    ordering = ('name',)
    filter_value = ''

    def get_queryset(self):
        qs = super().get_queryset()
        if self.filter_value:
            search = self.filter_value
            qs = qs.filter(
                Q(name__icontains=search)
                | Q(last_release__summary__icontains=search)
                | Q(last_release__description__icontains=search)
                | Q(last_release__keywords__name__icontains=search)
            )
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search'] = self.filter_value
        return context


class MapHomeView(View):
    def get(self, request, *args, **kwargs):
        view = MapListView.as_view(filter_value='')
        return view(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        view = MapListView.as_view(filter_value=self.request.POST['search'])
        request.method = 'GET'
        return view(request, *args, **kwargs)


class MapDetailView(ActiveMapMixin, DetailView):
    model = Map
    slug_field = 'slug'
    slug_url_kwarg = 'sname'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = _("Map %(map_name)s") % {'map_name': self.object.name}
        return ctx


class MapEditView(ActiveMapMixin, PermissionRequiredMixin, UpdateView):
    model = Map
    fields = ('name', 'slug', 'license')
    template_name_suffix = '_edit'
    permission_required = 'zeroadmaprepo.change_map'

    def get_form_class(self):
        user = self.request.user
        extra_fields = ['author', 'free_author'] if user.is_staff or user.is_superuser else []
        return modelform_factory(self.model, fields=(*self.fields, *extra_fields))

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = _("Edit %(map_name)s") % {'map_name': self.object.name}
        return ctx

    @transaction.atomic
    def form_valid(self, form):
        response = super().form_valid(form=form)
        for release in self.object.releases.all():
            with release.data.open('rb') as zip_file:
                map_parser = MapParser(zip_file)
                if map_parser.parse():
                    map_parser.map_settings['Name'] = self.object.name
                    map_parser.map_name = self.object.slug
                    map_parser.mod_license = self.object.license_type
                    pyromod = map_parser.build_pyromod(self.object)
                else:
                    pyromod = None
            if pyromod:
                release.data.save(
                    Release._meta.get_field('data').upload_to(release),
                    File(pyromod),
                )
        return response


class MapDeleteView(ActiveMapMixin, PermissionRequiredMixin, DeleteView):
    model = Map
    success_url = reverse_lazy('map-list')
    permission_required = 'zeroadmaprepo.delete_map'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = _("Delete %(map_name)s") % {'map_name': self.object.name}
        return ctx
