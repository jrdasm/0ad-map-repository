from django.views.generic.base import TemplateView
from pathlib import Path


class MarkDownView(TemplateView):
    template_name = 'zeroadmaprepo/markdown.html'
    md_filename = 'example.md'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        base_dir = Path(__file__).parent.parent
        context['mdtext'] = (base_dir / 'templates' / 'zeroadmaprepo' / self.md_filename).read_text()
        return context
