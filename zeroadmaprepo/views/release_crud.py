from django.core.files.base import File
from django.db import transaction
from django.http import StreamingHttpResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views import View
from django.views.generic import (
    DeleteView,
    DetailView,
    UpdateView,
)
from guardian.mixins import PermissionRequiredMixin

from ..forms import RlzEditForm
from ..map_parser import MapParser
from ..models import (
    Map,
    Release,
)


class ActiveReleaseMixin:
    def get_queryset(self):
        map_pk = self.kwargs.get('pk')
        map_slug = self.kwargs.get('sname')
        return super().get_queryset().filter(map__active=True, map__pk=map_pk, map__slug=map_slug)


class ReleaseDetailView(ActiveReleaseMixin, DetailView):
    model = Release
    pk_url_kwarg = ''  # to force to use the slug, i.e. version
    slug_field = 'version'
    slug_url_kwarg = 'version'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['map'] = self.object.map
        ctx['title'] = f"{self.object.map.name} - {self.object.version}"
        return ctx


class ReleaseDownloadView(View):
    def get(self, request, *args, **kwargs):
        map_pk = self.kwargs.get('pk')
        map_slug = self.kwargs.get('sname')
        version = self.kwargs.get('version')
        release = get_object_or_404(
            Release.objects.filter(
                map__active=True,
                map__pk=map_pk,
                map__slug=map_slug,
            ),
            version=version,
        )
        response = StreamingHttpResponse(release.data, content_type='application/zip')
        response['Content-Disposition'] = f'attachment; filename={map_slug}-{version}.pyromod'
        return response


class ReleaseEditView(ActiveReleaseMixin, PermissionRequiredMixin, UpdateView):
    model = Release
    form_class = RlzEditForm
    pk_url_kwarg = ''  # to force to use the slug, i.e. version
    slug_field = 'version'
    slug_url_kwarg = 'version'
    template_name_suffix = '_edit'
    permission_required = 'zeroadmaprepo.change_release'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = _("Edit release %(map_name)s %(version)s") % {
            'map_name': self.object.map.name,
            'version': self.object.version,
        }
        return ctx

    @transaction.atomic
    def form_valid(self, form):
        response = super().form_valid(form=form)
        with self.object.data.open('rb') as zip_file:
            map_parser = MapParser(zip_file)
            if map_parser.parse():
                map_parser.map_settings['Name'] = self.object.map.name
                map_parser.map_name = self.object.map.slug
                map_parser.mod_license = self.object.map.license_type
                map_parser.mod['version'] = self.object.version
                map_parser.mod['label'] = self.object.summary
                map_parser.mod['description'] = self.object.description
                map_parser.map_settings['Description'] = self.object.description
                map_parser.mod['dependencies'] = self.object.dependencies
                map_parser.mod['url'] = self.object.url
                map_parser.map_settings['Keywords'] = self.object.keywords.get_tag_list()
                pyromod = map_parser.build_pyromod(self.object.map)
            else:
                pyromod = None
        if pyromod:
            self.object.data.save(
                Release._meta.get_field('data').upload_to(self.object),
                File(pyromod),
            )
        return response


class ReleaseDeleteView(ActiveReleaseMixin, PermissionRequiredMixin, DeleteView):
    model = Release
    pk_url_kwarg = ''  # to force to use the slug, i.e. version
    slug_field = 'version'
    slug_url_kwarg = 'version'
    permission_required = 'zeroadmaprepo.delete_release'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        map = get_object_or_404(Map, pk=self.kwargs['pk'])
        ctx['only_one_left'] = map.releases.count() == 1
        ctx['title'] = _("Delete release %(map_name)s %(version)s") % {
            'map_name': self.object.map.name,
            'version': self.object.version,
        }
        return ctx

    @transaction.atomic
    def delete(self, request, *args, **kwargs):
        response = super().delete(request, *args, **kwargs)
        map = self.object.map
        last_release = map.releases.exclude(version=self.object.version).order_by('version').last()
        map.last_release = last_release
        map.save()
        return response

    def get_success_url(self):
        return reverse_lazy('map-detail', kwargs={
            'pk': self.object.map.pk,
            'sname': self.object.map.slug,
        })
