from django.urls import reverse_lazy
from django.views.generic.base import TemplateView


class SelectLangView(TemplateView):
    template_name = 'zeroadmaprepo/select_lang.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        # prevent url looping
        if self.request.build_absolute_uri() != self.request.META['HTTP_REFERER']:
            ctx['next_url'] = self.request.META['HTTP_REFERER']
        else:
            ctx['next_url'] = reverse_lazy('map-list')
        return ctx
