from base64 import b64encode
from io import BytesIO
from uuid import uuid1

from django.conf import settings
from django.core.files.base import File
from django.core.files.images import ImageFile
from django.db import transaction
from django.http import Http404
from django.shortcuts import redirect
from django.template.defaultfilters import slugify
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views import View
from django.views.generic import FormView
from guardian.mixins import PermissionRequiredMixin
from guardian.shortcuts import assign_perm

from ..forms import (
    MapCreateStep1Form,
    MapCreateStep2AdminForm,
    MapCreateStep2Form,
)
from ..map_parser import MapParser
from ..models import (
    Map,
    Release,
)


class MapCreatePermsMixin(PermissionRequiredMixin):
    permission_required = 'zeroadmaprepo.add_map'
    accept_global_perms = True


class MapCreateView(MapCreatePermsMixin, View):
    def get(self, request, *args, **kwargs):
        return redirect('map-create-step1', str(uuid1()))


class MapCreateStep1View(MapCreatePermsMixin, FormView):
    form_class = MapCreateStep1Form
    template_name = 'zeroadmaprepo/map_create_step1.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['uuid'] = self.kwargs['uuid']
        ctx['title'] = _("Add a map")
        return ctx

    def form_valid(self, form):
        uuid = self.kwargs['uuid']
        zip_file = form.cleaned_data['zip_file']
        zip_file_path = settings.TMP_ROOT / f"{uuid}.zip"
        with zip_file_path.open('wb') as f:
            for chunk_bytes in zip_file.chunks():
                f.write(chunk_bytes)
        print(f"form validation step1: {zip_file_path.name} saved", flush=True)
        zip_file.seek(0)
        map_parser = MapParser(zip_file)
        map_parser.parse()
        if map_parser.map_preview:
            map_preview_path = settings.TMP_ROOT / f"{uuid}.png"
            with map_preview_path.open('wb') as f:
                f.write(map_parser.map_preview.getvalue())
            print(f"form validation step1: {map_preview_path.name} saved", flush=True)
        self.request.session[uuid] = map_parser.to_json()
        print(f"{self.request.session[uuid]=}")
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('map-create-step2', kwargs=self.kwargs)


class MapCreateStep2View(MapCreatePermsMixin, FormView):
    form_class = MapCreateStep2Form
    template_name = 'zeroadmaprepo/map_create_step2.html'

    def get_form_class(self):
        user = self.request.user
        return MapCreateStep2AdminForm if user.is_staff or user.is_superuser else self.form_class

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        uuid = self.kwargs['uuid']
        map_preview_path = settings.TMP_ROOT / f"{uuid}.png"
        form.fields['new_thumbnail'].required = not map_preview_path.exists()
        if 'author_override' in form.fields:
            author_field = form.fields['author_override']
            author_field.help_text = author_field.help_text % dict(user_name=self.request.user.name)
        return form

    def get_initial(self):
        initial = super().get_initial()
        uuid = self.kwargs['uuid']
        zip_file_path = settings.TMP_ROOT / f"{uuid}.zip"
        if not zip_file_path.exists():
            raise Http404
        json = self.request.session[uuid]
        if json:
            initial['name'] = json['map_settings']['Name']
            initial['slug'] = slugify(json['map_name'])
            initial['category'] = json['map_cat']
            initial['license'] = json['mod_license']
            initial['summary'] = json['map_settings']['Description']
            if json['mod']:
                initial['description'] = json['mod']['description']
                initial['version'] = json['mod']['version']
                initial['dependencies'] = json['mod']['dependencies']
                initial['url'] = json['mod']['url']
            initial['keywords'] = json['map_settings']['Keywords']
        return initial

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        uuid = self.kwargs['uuid']
        ctx['uuid'] = uuid
        ctx['title'] = _("Add a map")
        json = self.request.session[uuid]
        ctx['warnings'] = json['warnings']
        map_preview_path = settings.TMP_ROOT / f"{uuid}.png"
        if map_preview_path.exists():
            map_preview_base64 = b64encode(map_preview_path.read_bytes()).decode('ascii')
            ctx['original_thumbnail'] = f"data:image/png;base64,{map_preview_base64}"
        else:
            ctx['original_thumbnail'] = None
        return ctx

    def form_valid(self, form):
        try:
            with transaction.atomic():
                uuid = self.kwargs['uuid']
                initial_json = self.request.session[uuid].copy()
                form_data = form.cleaned_data
                map_slug = form_data['slug']
                map_settings = initial_json['map_settings'].copy()
                map_settings.update({
                    'Name': form_data['name'],
                    'Description': form_data['summary'],
                    'Keywords': form_data['keywords'],
                    'Preview': f"{map_slug}.png",
                })
                if 'Script' in map_settings:
                    map_settings['Script'] = f"{map_slug}.js"
                mod = (initial_json['mod'] or {}).copy()
                mod.update({
                    'name': map_slug,
                    'label': form_data['summary'],
                    'description': form_data['description'],
                    'version': form_data['version'],
                    'dependencies': form_data['dependencies'],
                })
                if form_data['url']:
                    mod['url'] = form_data['url']
                json = {
                    'mod_format': initial_json['mod_format'],
                    'mod': mod,
                    'map_name': map_slug,
                    'map_ext': initial_json['map_ext'],
                    'map_cat': form_data['category'],
                    'mod_license': form_data['license'],
                    'zip_path_list': initial_json['zip_path_list'],
                    'zip_map_info': initial_json['zip_map_info'],
                    'map_settings': map_settings,
                    'map_preview_path': initial_json['map_preview_path'],
                    'script_path': initial_json['script_path'],
                    'warnings': initial_json['warnings'],
                }
                zip_file_path = settings.TMP_ROOT / f"{uuid}.zip"
                map_preview_path = settings.TMP_ROOT / f"{uuid}.png"
                free_author = form_data.get('author_override', '').strip()
                map = Map.objects.create(
                    name=form_data['name'],
                    slug=map_slug,
                    category=form_data['category'],
                    author=self.request.user if not free_author else None,
                    free_author=free_author if free_author else None,
                    license=form_data['license'],
                )
                with zip_file_path.open('rb') as zip_file:
                    if form_data['new_thumbnail']:
                        preview = form_data['new_thumbnail']
                    else:
                        preview = BytesIO(map_preview_path.read_bytes())
                    map_parser = MapParser(zip_file)
                    print(f"{json=}")
                    map_parser.from_json(json)
                    pyromod = map_parser.build_pyromod(map, preview)
                release = Release(
                    map=map,
                    version=form_data['version'],
                    summary=form_data['summary'],
                    description=form_data['description'],
                    dependencies=form_data['dependencies'],
                    url=form_data['url'],
                    keywords=form_data['keywords'],
                )
                release.data.save(
                    Release._meta.get_field('data').upload_to(release),
                    File(pyromod),
                    save=False,
                )
                release.thumbnail.save(
                    Release._meta.get_field('thumbnail').upload_to(release),
                    ImageFile(preview),
                    save=False,
                )
                release.save()
                map.last_release = release
                map.save()
                zip_file_path.unlink(missing_ok=True)
                map_preview_path.unlink(missing_ok=True)
                self.map = map
                assign_perm('zeroadmaprepo.change_map', self.request.user, map)
                assign_perm('zeroadmaprepo.delete_map', self.request.user, map)
                assign_perm('zeroadmaprepo.add_release', self.request.user, map)
                assign_perm('zeroadmaprepo.change_release', self.request.user, release)
                assign_perm('zeroadmaprepo.delete_release', self.request.user, release)
                return super().form_valid(form)
        except Exception as e:
            import traceback
            traceback.print_exception(type(e), e, e.__traceback__)
            error = str(e)
            field_name = None
            if 'UNIQUE' in error:
                if error.endswith('.name'):
                    field_name = 'name'
                elif error.endswith('.slug'):
                    field_name = 'slug'
                error = _("%(name)s is already taken") % {'name': field_name[0].upper() + field_name[1:]}
            form.add_error(field_name, error)
            return self.form_invalid(form)

    def get_success_url(self):
        return reverse_lazy('map-detail', kwargs={
            'pk': self.map.pk,
            'sname': self.map.slug,
        })
