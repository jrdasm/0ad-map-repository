function createPopup(linkIdOrElem, popupIdOrElem) {
  window.addEventListener('DOMContentLoaded', () => {
    const popupGlassPane = document.createElement("div");
    popupGlassPane.setAttribute("id", "popup-glass-pane");
    document.body.appendChild(popupGlassPane);
    const linkElem = typeof linkIdOrElem == "string" ? document.getElementById(linkIdOrElem) : linkIdOrElem;
    const popupElem = typeof popupIdOrElem == "string" ? document.getElementById(popupIdOrElem) : popupIdOrElem;
    popupElem.classList.add("popup");
    const closeButtons = popupElem.getElementsByClassName("popup-close");
    let popupClickListener;
    let popupCloseButtonListener;
    let windowEscapeListener;
    const closeFct = () => {
      popupGlassPane.classList.remove("show");
      popupElem.classList.remove("show");
      popupElem.removeEventListener("click", popupClickListener);
      window.removeEventListener("keyup", windowEscapeListener);
      if (closeButtons.length) {
        closeButtons[0].removeEventListener("click", popupCloseButtonListener);
      }
    }
    popupClickListener = e => {
      if (e.target == popupElem) {
        closeFct();
      }
    };
    popupCloseButtonListener = e => closeFct();
    windowEscapeListener = e => {
      if (e.key == "Escape") {
        closeFct();
      }
    };
    linkElem.addEventListener("click", e => {
      popupElem.addEventListener("click", popupClickListener);
      window.addEventListener("keyup", windowEscapeListener);
      if (closeButtons.length) {
        closeButtons[0].addEventListener("click", popupCloseButtonListener);
      }
      popupGlassPane.classList.add("show");
      popupElem.classList.add("show");
    });
  });
}
