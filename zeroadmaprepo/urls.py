from django.urls import (
    include,
    path,
    register_converter,
)

from .views import (
    HowToInstallView,
    MapCreateStep1View,
    MapCreateStep2View,
    MapCreateView,
    MapDeleteView,
    MapDetailView,
    MapEditView,
    MapHomeView,
    PolicyView,
    ReleaseCreateStep1View,
    ReleaseCreateStep2View,
    ReleaseCreateView,
    ReleaseDeleteView,
    ReleaseDetailView,
    ReleaseDownloadView,
    ReleaseEditView,
    SelectLangView,
    TosView,
)

register_converter(
    type('SemVerConverter', (), {
        'to_python': lambda self, value: value,
        'to_url': lambda self, value: value,
        'regex': r'\d+(?:\.\d+){0,2}',
    }),
    'semver',
)

urlpatterns = [
    path('', MapHomeView.as_view(), name='map-list'),
    path('i18n/', include('django.conf.urls.i18n')),
    path('lang/', SelectLangView.as_view(), name='lang-select'),
    path('accounts/', include('allauth.urls')),
    path('policy', PolicyView.as_view(), name='policy'),
    path('tos', TosView.as_view(), name='tos'),
    path('how-to-install', HowToInstallView.as_view(), name='how-to-install'),
    path('map/new-map', MapCreateView.as_view(), name='map-create'),
    path('map/new-map/step1/<uuid>', MapCreateStep1View.as_view(), name='map-create-step1'),
    path('map/new-map/step2/<uuid>', MapCreateStep2View.as_view(), name='map-create-step2'),
    path('map/<int:pk>-<slug:sname>', MapDetailView.as_view(), name='map-detail'),
    path('map/<int:pk>-<slug:sname>/edit', MapEditView.as_view(), name='map-edit'),
    path('map/<int:pk>-<slug:sname>/delete', MapDeleteView.as_view(), name='map-delete'),
    path('map/<int:pk>-<slug:sname>/new-release', ReleaseCreateView.as_view(), name='release-create'),
    path('map/<int:pk>-<slug:sname>/new-release/step1/<uuid>', ReleaseCreateStep1View.as_view(),
         name='release-create-step1'),
    path('map/<int:pk>-<slug:sname>/new-release/step2/<uuid>', ReleaseCreateStep2View.as_view(),
         name='release-create-step2'),
    path('map/<int:pk>-<slug:sname>/<semver:version>', ReleaseDetailView.as_view(), name='release-detail'),
    path('map/<int:pk>-<slug:sname>/<semver:version>.pyromod', ReleaseDownloadView.as_view(), name='release-download'),
    path('map/<int:pk>-<slug:sname>/<semver:version>/edit', ReleaseEditView.as_view(), name='release-edit'),
    path('map/<int:pk>-<slug:sname>/<semver:version>/delete', ReleaseDeleteView.as_view(), name='release-delete'),
]
