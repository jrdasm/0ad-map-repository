from contextlib import closing
from dataclasses import dataclass
from io import BytesIO
from json import (
    JSONDecodeError,
    dumps,
    loads,
)
from pathlib import Path
from re import match
from sys import version_info
from typing import (
    BinaryIO,
    Optional,
    TypeVar,
)
from zipfile import (
    ZIP_DEFLATED,
    BadZipFile,
    ZipFile,
)

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from lxml import etree
from wand.color import Color
from wand.image import Image

from .models import Map
from .models.category import CategoryType
from .models.license import LicenseType

if version_info >= (3, 10):
    from typing import TypeAlias
    JSON: TypeAlias = dict[str, 'JSON'] | list['JSON'] | str | int | float | bool
    Settings: TypeAlias = dict[str, 'Settings' | str]
else:
    from typing import Union
    JSON = Union[dict[str, 'JSON'], list['JSON'], str, int, float, bool]
    Settings = dict[str, Union['Settings', str]]
T = TypeVar("T", bound="ZipMapInfo")


@dataclass
class ZipMapInfo:
    path: Path
    name: Optional[str] = None
    cat: Optional[CategoryType] = None

    def to_json(self) -> JSON:
        return {
            'path': str(self.path),
            'name': self.name,
            'cat': self.cat.value if self.cat else None,
        }

    @classmethod
    def from_json(cls: type[T], json: JSON) -> T:
        return cls(
            path=Path(json.get('path', '')),
            name=json.get('name') or None,
            cat=CategoryType.from_value(json.get('cat')),
        )


class MapParserExceptionMixin:
    messages = {
        "no_map": _("File '%(filename)s' does not contains a 0 AD map"),
        "multiple_map": _("File '%(filename)s' contains more than one map"),
        "map_missing_pmp": _("File '%(filename)s' containing xml map '%(mapname)s' is missing pmp file"),
        "map_xml_bad_format": _("File '%(filename)s' contains a bad xml map format for '%(mapname)s': %(error)s"),
        "map_pmp_bad_format": _("File '%(filename)s' contains a bad pmp map format for '%(mapname)s': %(error)s'"),
        "map_json_bad_format": _("File '%(filename)s' contains a bad json map format for '%(mapname)s': %(error)s"),
        "map_bad_settings": _("File '%(filename)s' contains a map '%(mapname)s' with bad settings: %(error)s"),
        "map_bad_mod": _("File '%(filename)s' contains a map '%(mapname)s' with bad mod.json file: %(error)s"),
    }
    code = 'invalid_map_format'


class MapParserValidationException(MapParserExceptionMixin, ValidationError):
    def __init__(
        self,
        message_key: str,
        filename: Union[Path, str],
        map_name: Optional[str] = None,
        error: Optional[str] = None,
    ):
        super().__init__(
            self.messages.get(message_key),
            code=self.code,
            params={
                'filename': filename,
                'mapname': map_name,
                'error': error,
            },
        )


class Warnings(list):
    pass


class MapParser:
    """
    https://trac.wildfiregames.com/wiki/Modding_Guide#Wherearethemods
    https://trac.wildfiregames.com/wiki/Mod_Layout
    https://trac.wildfiregames.com/wiki/Modding_Guide#Distributingyourmods
    https://trac.wildfiregames.com/wiki/Modding_Guide#Howtoinstallmods
    ImageMagick can convert PNG back to DDS (with DXT1 compression) without the need to use the pyrogenesis binary
    But Pillow cannot do any compression (required by 0AD) right now
    https://github.com/python-pillow/Pillow/issues/4864
    """
    # https://trac.wildfiregames.com/wiki/Mod_Layout#maps
    mod_map_basepath = 'maps/'
    # https://trac.wildfiregames.com/wiki/PMP_File_Format#ScenarioXMLformat
    # https://trac.wildfiregames.com/wiki/PMP_File_Format#ScriptSettings
    mod_xml_regex = rf'^{mod_map_basepath}(skirmishes|scenarios|tutorials)/([^./]+)\.xml$'
    # https://trac.wildfiregames.com/wiki/Random_Map_Generator_Internals#DefiningtheMap
    mod_json_regex = rf'^{mod_map_basepath}(random)/([^./]+)\.json$'
    # https://trac.wildfiregames.com/wiki/Atlas_Manual_Map_Preview
    # https://github.com/0ad/0ad/blob/master/binaries/data/mods/public/gui/maps/MapCache.js
    mod_preview_basepath = 'art/textures/ui/session/icons/mappreview/'
    # https://github.com/0ad/0ad/blob/master/binaries/data/mods/public/gui/maps/MapCache.js
    preview_size = (400, 300)
    canvas_size = (512, 512)
    # This cannot be done right now with Pillow/PIL as compression is not supported
    # https://github.com/python-pillow/Pillow/issues/4864
    # https://pillow.readthedocs.io/en/stable/handbook/image-file-formats.html#dds
    # So wand will be used
    # https://github.com/0ad/0ad/blob/master/source/lib/tex/tex_dds.cpp
    # there is code for handling uncompressed data,
    # unfortunately the fourCC code read shoud watch DXTn where n could be 1, 3 or 5.
    # So only compression is supported.
    # DXT1 does not handle alpha, aka transparency, but it’s ok for map preview anyway.
    dds_compression = 'dxt1'

    def __init__(self, file_obj: BinaryIO):
        self.file_obj: BinaryIO = file_obj
        self.mod_format: bool = False
        self.mod: Optional[JSON] = None
        self.map_name: str = None
        self.map_ext: str = None
        self.map_cat: Optional[CategoryType] = None
        self.mod_license: Optional[LicenseType] = None
        self.zip_path_list: list[Path] = list()
        self.zip_maps_info: list[ZipMapInfo] = list()
        self.zip_map_info: ZipMapInfo = None
        self.map_settings: Settings = dict()
        self.map_preview_path: Optional[Path] = None
        self.map_preview: Optional[BytesIO] = None  # preview_size PNG
        self.script_path: Optional[Path] = None
        self.warnings: Warnings = Warnings()

    def _fill_mod_format(self, zf: ZipFile) -> None:
        self.mod_format = 'mod.json' in zf.namelist()

    def _fill_zip_path_list(self, zf: ZipFile) -> None:
        if self.mod_format:
            self.zip_path_list = [Path(name) for name in zf.namelist() if any((
                name.startswith(self.mod_map_basepath),
                name.startswith(self.mod_preview_basepath),
                '/' not in name,
            ))]
        else:
            self.zip_path_list = [Path(name) for name in zf.namelist() if '/' not in name and any((
                name.lower().endswith('.xml'),
                name.lower().endswith('.pmp'),
                name.lower().endswith('.json'),
                name.lower().endswith('.js'),
                name.lower().endswith('.png'),
            ))]

    def _fill_zip_maps_info(self, zf: ZipFile) -> None:
        if self.mod_format:
            self.zip_maps_info = [
                ZipMapInfo(
                    path,
                    name=(mxml or mjson).group(2),
                    cat=CategoryType.from_value((mxml or mjson).group(1)),
                )
                for path in self.zip_path_list if any((
                    (mxml := match(self.mod_xml_regex, str(path))),
                    (mjson := match(self.mod_json_regex, str(path))),
                ))
            ]
        else:
            self.zip_maps_info = [
                ZipMapInfo(path)
                for path in self.zip_path_list if path.stem != 'mod' and path.suffix in ('.xml', '.json')
            ]

    def _fill_map_settings(self, zf: ZipFile, random: bool) -> None:
        if random:
            self._fill_random_map_settings(zf)
        else:
            self._fill_scenario_map_settings(zf)

    def _fill_random_map_settings(self, zf: ZipFile) -> None:
        zipname = self.file_obj.name
        map_name = self.zip_map_info.path.stem
        with zf.open(self.zip_map_info.path) as f:
            error = _("bad json format")
            try:
                root_settings = loads(f.read().decode('utf8'))
                if 'settings' not in root_settings:
                    error = _("json does not look like a 0 A.D. map")
                    raise JSONDecodeError
            except JSONDecodeError:
                raise MapParserValidationException('map_json_bad_format', zipname, map_name, error)
            self.map_settings = root_settings['settings']

    def _fill_scenario_map_settings(self, zf: ZipFile) -> None:
        zipname = self.file_obj.name
        map_name = self.zip_map_info.path.stem
        map_bad_format_exception_kwargs = {
            'message_key': 'map_xml_bad_format',
            'filename': zipname,
            'map_name': map_name,
            'error': _("bad xml format"),
        }
        with zf.open(str(self.zip_map_info.path)) as f:
            if f.read(5) != b'<?xml':
                map_bad_format_exception_kwargs['params']['error'] = _("not an xml file")
                raise MapParserValidationException(**map_bad_format_exception_kwargs)
            f.seek(0)
            root = etree.parse(f).getroot()
            script_settings = root.find('ScriptSettings')
            if root.tag != 'Scenario' or not root.attrib.get('version', '') or script_settings is None:
                map_bad_format_exception_kwargs['params']['error'] = _("xml does not look like a 0 A.D. map")
                raise MapParserValidationException(**map_bad_format_exception_kwargs)
            try:
                self.map_settings = loads(script_settings.text)
            except JSONDecodeError:
                map_bad_format_exception_kwargs['params']['error'] = _(
                    "xml map does not contain valid ScriptSettings"
                )
                raise MapParserValidationException(**map_bad_format_exception_kwargs)
        pmp_path = self.zip_map_info.path.with_suffix('.pmp')
        if pmp_path not in self.zip_path_list:
            raise MapParserValidationException('map_missing_pmp', zipname, map_name)
        with zf.open(str(pmp_path)) as f:
            # https://trac.wildfiregames.com/wiki/PMP_File_Format#PMPformat
            if f.read(4) != b'PSMP':
                raise MapParserValidationException('map_pmp_bad_format', zipname, map_name, _("bad magic header"))

    def _guess_category(self) -> CategoryType:
        if self.map_ext == 'xml':
            if 'Script' in self.map_settings:
                return CategoryType.SCENARIO
            else:
                return CategoryType.SKIRMISH
        else:  # 'json'
            return CategoryType.RANDOM

    def _fill_mod_json(self, zf: ZipFile) -> None:
        zipname = self.file_obj.name
        map_name = self.zip_map_info.path.stem
        with zf.open('mod.json') as f:
            try:
                self.mod = loads(f.read().decode('utf-8'))
            except JSONDecodeError:
                raise MapParserValidationException('map_bad_mod', zipname, map_name, _("not in json format"))
        # https://trac.wildfiregames.com/wiki/Modding_Guide#Howtomakeyourmodshowupinthemodselectionscreen
        mod_mandatory_props = ('name', 'version', 'label', 'description', 'dependencies')
        if not all(self.mod.get(prop) is not None for prop in mod_mandatory_props):
            raise MapParserValidationException('map_bad_settings', zipname, map_name, _("missing mandatory settings"))
        mod_name = self.mod['name']
        # https://wildfiregames.com/forum/topic/24333-guide-for-publishing-mods-on-modio/?do=findComment&comment=372029
        # also allow upper case
        name_regex = r'^[a-zA-Z0-9][-_a-zA-Z0-9]+$'
        if not match(name_regex, mod_name):
            raise MapParserValidationException(
                'map_bad_settings', zipname, map_name,
                _("mod name should consist only of alphanumeric characters, underscore and dash"),
            )
        version_regex = r'^[0-9]+(\.[0-9]+){0,2}$'
        if not match(version_regex, self.mod['version']):
            raise MapParserValidationException(
                'map_bad_settings', zipname, map_name,
                _("mod version may only contain numbers and up to two periods"),
            )
        mod_deps = self.mod['dependencies']
        if not isinstance(mod_deps, list) or not all(
            match(f'{name_regex[:-1]}((=|<=?|>=?){version_regex[1:-1]})?$', dep) for dep in mod_deps
        ):
            raise MapParserValidationException(
                'map_bad_settings', zipname, map_name,
                _("mod dependencies format is incorrect, array of strings expected"),
            )

    def _fill_preview(self, zf: ZipFile) -> None:
        zipname = self.file_obj.name
        map_name = self.zip_map_info.path.stem
        # Preview is optional, but if defined and not empty it should point to an existing preview file
        if (preview := self.map_settings.get('Preview')):
            self.map_preview_path = Path(f"{self.mod_preview_basepath}{preview}") if self.mod_format else Path(preview)
            # also check for DirectDrawStructure file instead
            if Path(f"{self.map_preview_path}.cached.dds") in self.zip_path_list:
                self.map_preview_path = Path(f"{self.map_preview_path}.cached.dds")
            if self.map_preview_path not in self.zip_path_list:
                error = _("missing preview file defined in settings")
                if self.mod_format:
                    raise MapParserValidationException('map_bad_settings', zipname, map_name, error)
                else:
                    self.map_preview_path = None
                    self.map_settings.pop('Preview')
                    self.warnings.append(error)

    def _fill_script(self, zf: ZipFile) -> None:
        zipname = self.file_obj.name
        map_name = self.zip_map_info.path.stem
        # Script is optional, but if defined and not empty it should point to an existing js script
        if (script := self.map_settings.get('Script')):
            self.script_path = self.zip_map_info.path.parent / script if self.mod_format else Path(script)
            if self.script_path not in self.zip_path_list:
                raise MapParserValidationException(
                    'map_bad_settings', zipname, map_name,
                    _("missing script file defined in settings"),
                )

    def check_valid(self, only_cat: Optional[CategoryType] = None, read_preview: bool = False):
        """
        This also parse data from the zip file.
        This raises MapParserValidationException and/or fill self.warnings.
        """
        self.warnings.clear()
        try:
            zipname = self.file_obj.name
            with ZipFile(self.file_obj) as zf:
                self._fill_mod_format(zf)
                self._fill_zip_path_list(zf)
                self._fill_zip_maps_info(zf)
                if len(self.zip_maps_info) == 0:
                    raise MapParserValidationException('no_map', zipname)
                elif len(self.zip_maps_info) > 1:
                    raise MapParserValidationException('multiple_map', zipname)
                self.zip_map_info = self.zip_maps_info[0]
                self.map_ext = self.zip_map_info.path.suffix.lower()[1:]
                if self.mod_format:
                    if self.zip_map_info.cat is None:
                        raise MapParserValidationException(
                            'map_bad_mod', zipname, self.zip_map_info.name,
                            _("map not in a known diretory/type"),
                        )
                    if only_cat and self.zip_map_info.cat != only_cat:
                        raise MapParserValidationException(
                            'map_bad_mod', zipname, self.zip_map_info.name,
                            _("map should be of %(category)s type") % {'category': only_cat.value},
                        )
                    self._fill_map_settings(zf, random=self.zip_map_info.cat == CategoryType.RANDOM)
                else:
                    if only_cat and any((
                        self.map_ext == 'json' and only_cat != CategoryType.RANDOM,
                        self.map_ext == 'xml' and only_cat == CategoryType.RANDOM,
                    )):
                        raise MapParserValidationException(
                            'map_bad_mod', zipname, self.zip_map_info.name,
                            _("map should be of %(category)s type") % {'category': only_cat.value},
                        )
                    self._fill_map_settings(zf, random=self.map_ext == 'json')
                mandatory_props = ('Name', 'Description', 'CircularMap')
                if not all(self.map_settings.get(prop) is not None for prop in mandatory_props):
                    raise MapParserValidationException(
                        'map_bad_settings', zipname, self.zip_map_info.name,
                        _("missing mandatory settings"),
                    )
                self.map_cat = self.zip_map_info.cat if self.mod_format else self._guess_category()
                if self.mod_format:
                    self._fill_mod_json(zf)
                self._fill_preview(zf)
                self._fill_script(zf)
                if read_preview and self.map_preview_path:
                    with (
                        zf.open(str(self.map_preview_path)) as f,
                        closing(Image(file=f)) as img,
                        closing(img[:self.preview_size[0], :self.preview_size[1]]) as imgCropped,
                    ):
                        self.map_preview = BytesIO()
                        imgCropped.format = 'PNG'
                        imgCropped.save(file=self.map_preview)
                self.map_name = self.zip_map_info.name if self.mod_format else self.map_settings['Name']
        except BadZipFile as e:
            raise ValidationError(str(e))

    def is_valid(self) -> bool:
        try:
            self.check_valid()
            return True
        except ValidationError:
            return False

    def parse(self) -> bool:
        try:
            self.check_valid(read_preview=True)
            return True
        except ValidationError as e:
            print(e)
            return False

    def to_json(self) -> JSON:
        return {
            'mod_format': self.mod_format,
            'mod': self.mod,
            'map_name': self.map_name,
            'map_ext': self.map_ext,
            'map_cat': self.map_cat.value if self.map_cat else None,
            'mod_license': self.mod_license.value if self.mod_license else None,
            'zip_path_list': [str(p) for p in self.zip_path_list],
            'zip_map_info': self.zip_map_info.to_json() if self.zip_map_info else None,
            'map_settings': self.map_settings,
            'map_preview_path': str(self.map_preview_path) if self.map_preview_path else None,
            'script_path': str(self.script_path) if self.script_path else None,
            'warnings': [str(w) for w in self.warnings],
        }

    def from_json(self, json: JSON) -> None:
        props = (
            'mod_format', 'mod', 'map_name', 'map_ext', 'map_cat', 'mod_license',
            'zip_path_list', 'zip_map_info', 'map_settings', 'map_preview_path', 'script_path',
            'warnings',
        )
        if all(prop in json for prop in props):
            self.mod_format = json['mod_format']
            self.mod = json['mod']
            self.map_name = json['map_name']
            self.map_ext = json['map_ext']
            self.map_cat = CategoryType.from_value(json['map_cat'])
            self.mod_license = LicenseType.from_value(json['mod_license'])
            self.zip_path_list = [Path(p) for p in json['zip_path_list']]
            self.zip_map_info = ZipMapInfo.from_json(json['zip_map_info']) if json['zip_map_info'] else None
            self.map_settings = json['map_settings']
            self.map_preview_path = Path(json['map_preview_path']) if json['map_preview_path'] else None
            self.script_path = Path(json['script_path']) if json['script_path'] else None
            self.warnings = Warnings(json['warnings'])
            print("loaded")

    def build_pyromod(self, map: Map, preview: BytesIO = None) -> BytesIO:
        if self.map_cat is None:
            raise ValueError("parse should be called first")
        if not self.mod:
            raise ValueError("self.mod should be filled")
        if preview:
            self.map_preview = preview
        self.map_settings['Preview'] = f'{self.map_name}.png'
        if self.script_path:
            self.map_settings['Script'] = f'{self.map_name}.js'
        self.mod['name'] = self.map_name
        if self.mod_license:
            self.mod['license'] = self.mod_license.name
        mod = BytesIO()
        with (
            ZipFile(self.file_obj) as zfin,
            ZipFile(mod, mode='w', compression=ZIP_DEFLATED) as zfout,
        ):
            zfout.comment = f"0 A.D. map: {self.map_name}".encode('utf-8')
            # keep only root files with no extension, or .md/.txt extensions
            lst = (name for name in zfin.namelist() if '/' not in name and any((
                '.' not in name and name != 'LICENSE',
                name.lower().endswith('.md'),
                name.lower().endswith('.txt'),
            )))
            for name in lst:
                with (
                    zfin.open(name) as fin,
                    zfout.open(name, mode='w') as fout,
                ):
                    fout.write(fin.read())
            if self.map_cat == CategoryType.RANDOM:
                final_json_path = Path(self.mod_map_basepath) / self.map_cat.value / f"{self.map_name}.json"
                json = loads(self.zip_map_info['path'].read_text(encoding='utf-8'))
                json['settings'] = self.map_settings
                zfout.writestr(str(final_json_path), dumps(json, ensure_ascii=False, indent=2))
            else:
                final_xml_path = Path(self.mod_map_basepath) / self.map_cat.value / f"{self.map_name}.xml"
                with (
                    zfin.open(str(self.zip_map_info.path)) as fin,
                    zfout.open(str(final_xml_path), mode='w') as fout,
                ):
                    xml = etree.parse(fin)
                    root = xml.getroot()
                    script_settings = root.find('ScriptSettings')
                    script_settings.text = dumps(self.map_settings, ensure_ascii=False, indent=2)
                    xml.write(fout, encoding='utf-8', xml_declaration=True)
                final_pmp_path = Path(self.mod_map_basepath) / self.map_cat.value / f"{self.map_name}.pmp"
                with (
                    zfin.open(str(self.zip_map_info.path.with_suffix('.pmp'))) as fin,
                    zfout.open(str(final_pmp_path), mode='w') as fout,
                ):
                    fout.write(fin.read())
            if self.script_path:  # could be part of random, scenarios or tutorials map
                final_script_path = Path(self.mod_map_basepath) / self.map_cat.value / f"{self.map_name}.js"
                with (
                    zfin.open(str(self.script_path)) as fin,
                    zfout.open(str(final_script_path), mode='w') as fout,
                ):
                    fout.write(fin.read())
            final_map_preview_path = Path(f"{self.mod_preview_basepath}{self.map_name}.png.cached.dds")
            self.map_preview.seek(0)
            with (
                zfout.open(str(final_map_preview_path), mode='w') as f,
                closing(Image(file=self.map_preview)) as img,
                Color('black') as color_bg,
            ):
                # https://www.imagemagick.org/script/command-line-processing.php#geometry
                img.transform(resize='x'.join(str(size) for size in self.preview_size))
                img.background_color = color_bg
                img.extent(*self.canvas_size)
                img.format = 'dds'
                img.compression = self.dds_compression
                img.save(file=f)
            self.map_preview.seek(0)
            if self.mod_license:
                zfout.writestr('LICENSE', self.mod_license.text(map))
            zfout.writestr('mod.json', dumps(self.mod, ensure_ascii=False, indent=2))
        return mod

    def __repr__(self) -> str:
        ret = f"{self.map_name} ({'mod' if self.mod_format else 'plain'}):"
        ret += f" {self.map_cat.value if self.map_cat else 'No-cat'} ({self.map_ext})"
        if self.mod_license:
            ret += f" license {self.mod_license.name}"
        if self.map_preview_path:
            ret += " with preview"
        if self.script_path:
            ret += " with script"
        return ret
