#!/bin/sh
# The entrypoint will source /app/prestart.sh if it exists before launching the app.
set -e
# Tell nginx to serve static files (cannot be done before as nginx.conf is generated)
sed -i -r '/location \/ /i \
    location /static/ {\
        root /app/;\
    }\
    location /maps/ {\
        root /app/;\
    }\
    location /favicon.ico { # for direct access\
        alias /app/zeroadmaprepo/static/favicon.ico;\
    }\
' /etc/nginx/conf.d/nginx.conf
cd /app
maxsec=120
echo "*** Wait for database to be ready (max ${maxsec} seconds)"
dbpk=0
for i in $(seq $maxsec); do
  if echo '' | pipenv run python ./manage.py dbshell 2>/dev/null; then
    dbok=1
    break
  fi
  sleep 1
done
if [ $dbok -eq 1 ]; then
  echo "*** Django Migrate"
  pipenv run python ./manage.py migrate
  export VENV=$(pipenv --venv)
else
  # allow regular error
  echo '' | pipenv run python ./manage.py dbshell
fi
