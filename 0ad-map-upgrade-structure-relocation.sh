#!/bin/sh
# https://code.wildfiregames.com/D1009
sed -i \
  -e 's,gaia/flora_bush_badlands,gaia/tree/bush_badlands,g' \
  -e 's,gaia/flora_bush_temperate_winter,gaia/tree/bush_temperate_winter,g' \
  -e 's,gaia/flora_bush_temperate,gaia/tree/bush_temperate,g' \
  -e 's,gaia/flora_bush_tropic,gaia/tree/bush_tropic,g' \
  -e 's,gaia/flora_bush_berry_desert,gaia/fruit/berry_05,g' \
  -e 's,gaia/flora_bush_berry_autumn_01,gaia/fruit/berry_04,g' \
  -e 's,gaia/flora_bush_berry_03,gaia/fruit/berry_03,g' \
  -e 's,gaia/flora_bush_berry_02,gaia/fruit/berry_02,g' \
  -e 's,gaia/flora_bush_berry,gaia/fruit/berry_01,g' \
  -e 's,gaia/flora_bush_grapes,gaia/fruit/grapes,g' \
  -e 's,gaia/flora_tree_apple,gaia/fruit/apple,g' \
  -e 's,gaia/flora_tree_banana,gaia/fruit/banana,g' \
  -e 's,gaia/flora_tree_date_palm_fruit,gaia/fruit/date,g' \
  -e 's,gaia/flora_tree_fig,gaia/fruit/fig,g' \
  -e 's,gaia/flora_tree_olive,gaia/fruit/olive,g' \
  -e 's,gaia/flora_tree_,gaia/tree/,g' \
  "$1"
# https://code.wildfiregames.com/D1010
sed -i \
  -e 's,gaia/geology_metal_alpine_slabs,gaia/ore/alpine_large,g' \
  -e 's,gaia/geology_metal_alpine,gaia/ore/alpine_small,g' \
  -e 's,gaia/geology_metal_desert_badlands_slabs,gaia/ore/badlands_large,g' \
  -e 's,gaia/geology_metal_desert_slabs,gaia/ore/desert_large,g' \
  -e 's,gaia/geology_metal_desert_small,gaia/ore/desert_small,g' \
  -e 's,gaia/geology_metal_greek,gaia/ore/greece_small,g' \
  -e 's,gaia/geology_metal_mediterranean_slabs,gaia/ore/mediterranean_large,g' \
  -e 's,gaia/geology_metal_mediterranean,gaia/ore/mediterranean_small,g' \
  -e 's,gaia/geology_metal_savanna_slabs,gaia/ore/savanna_large,g' \
  -e 's,gaia/geology_metal_temperate_slabs,gaia/ore/temperate_large,g' \
  -e 's,gaia/geology_metal_temperate,gaia/ore/temperate_small,g' \
  -e 's,gaia/geology_metal_tropic_slabs,gaia/ore/tropical_large,g' \
  -e 's,gaia/geology_metal_tropic,gaia/ore/tropical_small,g' \
  -e 's,gaia/geology_stone_alpine_a,gaia/rock/alpine_small,g' \
  -e 's,gaia/geology_stone_desert_quarried,gaia/rock/desert_cut,g' \
  -e 's,gaia/geology_stone_desert_small,gaia/rock/desert_small,g' \
  -e 's,gaia/geology_stone_greek,gaia/rock/greece_small,g' \
  -e 's,gaia/geology_stone_mediterranean,gaia/rock/mediterranean_small,g' \
  -e 's,gaia/geology_stone_savanna_quarried,gaia/rock/savanna_cut,g' \
  -e 's,gaia/geology_stone_savanna_small,gaia/rock/savanna_small,g' \
  -e 's,gaia/geology_stone_temperate,gaia/rock/temperate_small,g' \
  -e 's,gaia/geology_stone_tropic_a,gaia/rock/tropical_small,g' \
  -e 's,gaia/geology_stonemine_alpine_quarry,gaia/rock/alpine_large,g' \
  -e 's,gaia/geology_stonemine_desert_badlands_quarry,gaia/rock/badlands_large,g' \
  -e 's,gaia/geology_stonemine_desert_quarry,gaia/rock/desert_large,g' \
  -e 's,gaia/geology_stonemine_medit_quarry,gaia/rock/mediterranean_large,g' \
  -e 's,gaia/geology_stonemine_savanna_quarry,gaia/rock/savanna_large,g' \
  -e 's,gaia/geology_stonemine_temperate_formation,gaia/rock/temperate_large_02,g' \
  -e 's,gaia/geology_stonemine_temperate_granite_quarry,gaia/rock/temperate_large_03,g' \
  -e 's,gaia/geology_stonemine_temperate_quarry,gaia/rock/temperate_large,g' \
  -e 's,gaia/geology_stonemine_tropic_quarry,gaia/rock/tropical_large,g' \
  "$1"
# https://code.wildfiregames.com/D2234
sed -i \
  -e 's,other/palisades_rocks,structures/palisades,g' \
  -e 's,other/palisades_angle_spike,structures/palisades_spike_angle,g' \
  -e 's,other/palisades_small_spikes,structures/palisades_spikes_small,g' \
  -e 's,other/palisades_tall_spikes,structures/palisades_spikes_tall,g' \
  "$1"
# https://code.wildfiregames.com/D2254
sed -i \
  -e 's,gaia/fauna_hawk,birds/buzzard,g' \
  "$1"
# https://code.wildfiregames.com/D2774
sed -i \
  -e 's,gaia/fauna_fish_tuna,gaia/fish/tuna,g' \
  -e 's,gaia/fauna_fish_tilapia,gaia/fish/tilapia,g' \
  -e 's,gaia/fauna_fish,gaia/fish/generic,g' \
  "$1"
# https://code.wildfiregames.com/D3012
sed -i \
  -e 's,gaia/fauna_crocodile,gaia/fauna_crocodile_nile,g' \
  -e 's,gaia/fauna_mastiff,gaia/fauna_dog_mastiff,g' \
  -e 's,gaia/fauna_wolfhound,gaia/fauna_dog_wolfhound,g' \
  -e 's,gaia/fauna_pony,gaia/fauna_horse_pony,g' \
  -e 's,gaia/fauna_rhino,gaia/fauna_rhinoceros_white,g' \
  -e 's,gaia/fauna_arctic_wolf,gaia/fauna_wolf_arctic,g' \
  "$1"
# https://code.wildfiregames.com/D3097
sed -i \
  -e 's,other/,structures/,g' \
  "$1"
# https://wildfiregames.com/forum/topic/36649-upgrading-a23-skirmish-maps-to-be-compatible-with-a24/
sed -i \
  -e 's,structures/athen_,structures/athen/,g' \
  -e 's,structures/brit_,structures/brit/,g' \
  -e 's,structures/cart_,structures/cart/,g' \
  -e 's,structures/gaul_,structures/gaul/,g' \
  -e 's,structures/iber_,structures/iber/,g' \
  -e 's,structures/kush_,structures/kush/,g' \
  -e 's,structures/mace_,structures/mace/,g' \
  -e 's,structures/maur_,structures/maur/,g' \
  -e 's,structures/pers_,structures/pers/,g' \
  -e 's,structures/ptol_,structures/ptol/,g' \
  -e 's,structures/rome_,structures/rome/,g' \
  -e 's,structures/sele_,structures/sele/,g' \
  -e 's,structures/spart_,structures/spart/,g' \
  -e 's,units/athen_,units/athen/,g' \
  -e 's,units/brit_,units/brit/,g' \
  -e 's,units/cart_,units/cart/,g' \
  -e 's,units/gaul_,units/gaul/,g' \
  -e 's,units/iber_,units/iber/,g' \
  -e 's,units/kush_,units/kush/,g' \
  -e 's,units/mace_,units/mace/,g' \
  -e 's,units/maur_,units/maur/,g' \
  -e 's,units/pers_,units/pers/,g' \
  -e 's,units/ptol_,units/ptol/,g' \
  -e 's,units/rome_,units/rome/,g' \
  -e 's,units/sele_,units/sele/,g' \
  -e 's,units/spart_,units/spart/,g' \
  "$1"
# some we found ourselves
sed -i \
  -e 's,javelinist,javelineer,g' \
  -e 's,structures/special_treasure_,gaia/treasure/,g' \
  "$1"
